## python 中的魔法函数

'''
__xxx__ 这种函数叫做python中的魔法函数，这些函数都是一些独立的存在，当我们在定义一个类的时候，
加入这些函数可以增强我们的类，让我们在使用上更加方便。
比如，当一个类中声明了__abs__这个魔法函数，当用abs(xxx)会调用这个函数的返回结果
'''

'''
__str__定义到类中之后，当print(company)这个类的时候会输出这个魔法函数的值
__repr__是开发环境中的一个调用选项，IDE中可以看不出来，可以在Jupyter notebook查看
'''


class Company(object):
    def __init__(self, employee_list):
        self.employee_list = employee_list

    def __str__(self):
        return '.'.join(self.employee_list)

    def __repr__(self):
        return '.'.join(self.employee_list)

    def __abs__(self):
        return 123


company = Company(['tom', 'bob', 'jane'])
company  # 调用__repr__
print(company)  # 调用__str__
print(abs(company))  # 调用__abs__

'''
__len__顾名思义是为len()方法而生的，这里须知，len()方法不是简单的遍历对象得到总数，
python对其作了很多优化，其实一个list中内部有计数器来记录当前长度，因此len()效率很高
'''
