'''
python的面向对象更加彻底

函数和类也是属性，属于python的一等公民
1. 赋值给一个变量
2. 可以添加到集合对象中
3. 可以作为参数传递给函数
4. 可以当做函数的返回值

'''


def ask(name='Child'):
    print(name)


class Person:
    def __init__(self):
        print('person')


my_ask = ask
my_person = Person

obj_list = []
obj_list.append(my_ask)
obj_list.append(my_person)

for item in obj_list:
    item()

# type、object和class的关系
'''
type是一个类，同时也是一个对象
object是最顶层基类

1是int的实例，int是type的实例
obj是class的实例，class是type的实例

视频中有一幅图做描述
'''


class Student:
    pass


class MyStudent(Student):
    pass


stu = Student()
print(type(stu))
print(type(Student))
print(int.__bases__)
print(str.__bases__)
print(Student.__bases__)
print(MyStudent.__bases__)
print(type.__bases__)
print(object.__bases__)
print(type(object))

# id() 查看对象在内存中的地址
a = 1
print(id(1))

'''
None 在全局中只有一个，任何声明的Node类型都会指向这个全局唯一的Node类型
'''
q = None
w = None
print(id(q) == id(w))  # True