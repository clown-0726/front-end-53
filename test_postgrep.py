import boto3
from botocore.errorfactory import ClientError
import json
import os
import sys
import re
import datetime
import time
import uuid
import psycopg2

# sql = '''
#     INSERT INTO vendor_news_crawler_logs(id, total_amount, correct_amount, error_amount, year, month, day, old_flag, gen_date)
#     VALUES ('{id}', {total_amount}, {correct_amount}, {error_amount}, '{year}', '{month}', '{day}', {old_flag}, NOW())
#     '''.format(id=str(id),
#                total_amount=29008,
#                correct_amount=29008,
#                error_amount=29008,
#                year='2018',
#                month='08',
#                day='02',
#                old_flag=1
#                )

# db_meta = {
#     "database": "dataanalysislogs",
#     "user": "annotations",
#     "password": "Glaucusis12",
#     "host": "annotations-orbit-test.cx45rmx2ybfy.us-west-2.rds.amazonaws.com",
#     "port": "5432"
# }

db_meta = {
    "database": "linkedin_info",
    "user": "mmt",
    "password": "mmtuser",
    "host": "localhost",
    "port": "5432"
}


def action_db(sql):
    # Get connection
    try:
        conn = psycopg2.connect(database=db_meta["database"], user=db_meta["user"], password=db_meta["password"], host=db_meta["host"], port=db_meta["port"])
        print("Opened database successfully")
    except Exception as e:
        return "failed"

    # Start taking ddl actions
    try:
        cur = conn.cursor()
        cur.execute(sql)
        conn.commit()
        return 'success'
    except Exception as e:
        print(e)
        return "failed"
    finally:
        if conn:
            conn.close()


def search_db(sql):
    # Get connection
    try:
        conn = psycopg2.connect(database=db_meta["database"], user=db_meta["user"], password=db_meta["password"], host=db_meta["host"], port=db_meta["port"])
        print("Opened database successfully")
    except Exception as e:
        return "failed"

    # Start searching data
    try:
        cur = conn.cursor()
        cur.execute(sql)
        rows = cur.fetchall()
        return rows
    except Exception as e:
        print(e)
        return "failed"
    finally:
        if conn:
            conn.close()


sql = "select * from comp_info"
sql = "update set status='123' where id = 4"

rows = search_db(sql)
rows = action_db(sql)
print(rows)
